/// <reference path="../typings/tsd.d.ts" />

import React = require("react");
import SomeObject = require("./SomeObject.tsx")

export interface ISomeListProps {
    someObjects: ISomeObject[];
}

export class SomeListComponent extends React.Component<ISomeListProps, any> {
    private handleClick: () => void = () => {
        this.setState({someObjects: [{name: "Item1"}, {name: "Item2"}]});
        alert("This is a test");
    };

    public render() {
        return (
            <div>
                <ol>
                    {this.props.someObjects.map(obj => <SomeObject.SomeObjectComponent someObject={obj} />)}
                </ol>
                <div onClick={this.handleClick}>Add item</div>
            </div>
        );
    }
}
