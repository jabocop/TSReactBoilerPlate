/// <reference path="../typings/tsd.d.ts" />

import React = require("react");
import SomeList = require("./SomeList.tsx");


export interface IAppProps {
}

export default class App extends React.Component<IAppProps, any> {
    render() {
        return (
            <div>
                <SomeList.SomeListComponent someObjects={[{name: "Itm1"}, {name: "Itm2"}]}/>
            </div>
        );
    }
}
