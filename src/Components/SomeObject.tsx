/// <reference path="../typings/tsd.d.ts" />

import React = require("react");

export interface ISomeObjectProps {
    someObject: ISomeObject;
}

export class SomeObjectComponent extends React.Component<ISomeObjectProps, any> {
    render() {
        return (
            <li>{this.props.someObject.name}</li>
        );
    }
}
