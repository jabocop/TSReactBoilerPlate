# TSReactBoilerPlate 
## Instructions
A boilerplate for React with typescript using webpack.
Use _npm start_ for starting a development environment.
Use _npm run build_ to build production code.

## Credits
Inspired by [this project](https://github.com/christianalfoni/webpack-express-boilerplate.git)
